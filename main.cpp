/*
// Based on sample script tess.cpp - Recognize text on an image using Tesseract API and print it to the screen
*/

#include <tesseract/baseapi.h>
#include <tesseract/strngs.h>
#include <tesseract/resultiterator.h>
#include <leptonica/allheaders.h>
#include <iostream>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
// #include <QtCore/QString>

using namespace cv;

using namespace std;

int main(int argc, char** argv)
{
    if (argc < 2)
    {
        std::cout << "Please specify the input image!" << std::endl;
        return -1;
    }

    //    char *outText;
    int fontface = FONT_HERSHEY_SIMPLEX;
    tesseract::TessBaseAPI *api = new tesseract::TessBaseAPI();
    // Initialize tesseract-ocr with English, without specifying tessdata path
    if (api->Init(NULL, "eng")) {
        fprintf(stderr, "Could not initialize tesseract.\n");
        exit(1);
    }

    // Open input image with leptonica library
    Pix *image = pixRead(argv[1]);
    Mat im=imread(argv[1],CV_LOAD_IMAGE_COLOR);
    int icols=im.cols;
    int irows=im.rows;
    cv::Mat wim( irows, icols, im.type(), cv::Scalar(0));
    Rect letterRect;
    Mat src, dst, color_dst;
    int confidence_level=0;
    int j=0;
    src=imread(argv[1],CV_LOAD_IMAGE_GRAYSCALE);
    Canny(src, dst, 50, 200, 3 );
    cvtColor( dst, color_dst, CV_GRAY2BGR );
    vector<Vec4i> lines;
    HoughLinesP( dst, lines, 1, CV_PI/180, 80, 80, 10 );
    int numlines=0;

    // let's output an SVG!
    printf("<svg version=\"1.1\" baseProfile=\"full\" width=\"%d\" height=\"%d\" xmlns=\"http://www.w3.org/2000/svg\">\n", icols, irows);
    printf("<style>\n");
    printf("/* <![CDATA[ */\n");
    printf("line { fill: none; stroke: #00FF00; stroke-width: 1px; }\n");
    printf("rect { fill: none; stroke: #FF00FF; stroke-width: 1px; }\n");
    printf("text { font-size: 12; font-family: sans-serif; fill: #000000; dominant-baseline: text-after-edge }\n");
    printf("/* ]]> */\n");
    printf("</style>\n");

    // OUTPUT lines group
    printf("<g class=\"texturehoughhorizontallines\">\n");
    for( size_t i = 0; i < lines.size(); i++ )
    {
        double Angle = atan2(lines[i][3]- lines[i][1], lines[i][2]- lines[i][0]) * 180.0 / CV_PI;
        // printf("Angle:%0.03f", Angle);
        if( abs(Angle)>=0 && abs(Angle)<=5 ) {
            numlines++;
            printf("    <line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" />\n", lines[i][0], lines[i][1], lines[i][2], lines[i][3]);
            // line( color_dst, Point(lines[i][0], lines[i][1]), Point(lines[i][2], lines[i][3]), Scalar(0,0,255), 3, 8 );
        }
    }
    printf("</g>\n");

    if(numlines>40){
    // if(0) {
        printf("<g class=\"texturesymbols\">\n");

        // DETECT WITH TESSERACT!
        //Mat screen=cvCreateMat(im.rows,im.cols,im.type());
        api->SetImage(image);
        api->Recognize(0);
        //outText = api->GetUTF8Text();
        tesseract::ResultIterator* ri = api->GetIterator();
        tesseract::PageIteratorLevel level = tesseract::RIL_SYMBOL;

        if (ri != 0) {
            do {
                const char* word = ri->GetUTF8Text(level);
                float conf = ri->Confidence(level);
                int x1, y1, x2, y2;
                ri->BoundingBox(level, &x1, &y1, &x2, &y2);
//                printf("word: '%s';  \tconf: %.2f; BoundingBox: %d,%d,%d,%d;\n",
//                       word, conf, x1, y1, x2, y2);
                bool ignore;
                int psize,fid;
                ri->WordFontAttributes(&ignore,&ignore,&ignore,&ignore,&ignore,&ignore,&psize,&fid);
                //            printf("point size: %d font id: %d",psize, fid);
                if(conf>50){
                    // QString utfw = QString::fromUtf8(word);
                    printf("  <rect x=\"%d\" y=\"%d\" width=\"%d\" height=\"%d\"/>\n", x1, y1, (x2-x1), (y2-y1));
                    // printf("  <g class=\"word\" transform=\"translate(%d,%d)\"><rect x=\"0\" y=\"0\" width=\"%d\" height=\"%d\"/><text x=\"0\" y=\"%d\" style=\"font-size: %d\"><![CDATA[%s]]></text></g>\n", x1, y1, (x2-x1), (y2-y1), 0, (y2-y1), word);
                    printf("  <g class=\"word\" transform=\"translate(%d,%d)\"><rect x=\"0\" y=\"0\" width=\"%d\" height=\"%d\"/><text x=\"0\" y=\"0\"><![CDATA[%s]]></text></g>\n", x1, y1, (x2-x1), (y2-y1), word);
//                    if(utfw.size()>4){
//               //         printf("word: '%s'  \tlength: %d\n",word,utfw.length());
                        letterRect=CvRect();
                        letterRect.x=x1;
                        letterRect.y=y1;
                        letterRect.width=(x2-x1);
                        letterRect.height=(y2-y1);

                        confidence_level+=conf;
                        j++;
                    }
                //}
                delete[] word;
            } while (ri->Next(level));
        }
        printf("</g>\n");
    }
    //    printf("OCR output:\n%s", outText);
//    printf("num lines: %d \n",numlines);
//    printf("num symbols: %d \n",j);
//    printf("confidence level: %d \n",confidence_level);


    /*
    if(numlines>40){
        printf("%d\n",(confidence_level/j));
        imwrite(argv[2],wim);
    }else{
        printf("%d\n",0);
        imwrite(argv[2],wim);
    }
    */
//    namedWindow( "Detected Lines", 1 );
//    imshow( "Detected Lines", color_dst );
//    imshow( "Result", im);
//    imshow( "Wim", wim);
//    imwrite("words.jpg",wim);

//    waitKey();
    // Destroy used object and release memory
    api->End();
    //    delete [] outText;
    pixDestroy(&image);
    printf("</svg>\n");

    return 0;
}
