texture
=========

S I C V 2 0 1 6


Building
=========

Install requirements

	apt-get install libtesseract-dev libleptonica-dev tesseract-ocr-deu tesseract-ocr-nor tesseract-ocr-eng tesseract-ocr-fra cmake

Build with cmake, from the project folder:

	mkdir build
	cd build
	cmake ..
	make

